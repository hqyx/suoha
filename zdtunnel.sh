#!/bin/bash
# onekey zdtunnel
linux_os=("Debian" "Ubuntu" "CentOS" "Fedora" "Alpine")
#linux_update=("apt update" "apt update" "yum -y update" "yum -y update" "apk update")
#linux_install=("apt -y install" "apt -y install" "yum -y install" "yum -y install" "apk add -f")
green() { echo -e "\033[32m\033[01m$1\033[0m"; }
tunnel_home="/opt/tunnel"
n=0
for i in $(echo ${linux_os[@]}); do
	if [ $i == $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') ]; then
		break
	else
		n=$(($n + 1))
	fi
done
if [ $n == 5 ]; then
	echo 当前系统$(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2)没有适配
	echo 默认使用APT包管理器
	n=0
fi
if [ -z $(type -P unzip) ]; then
	${linux_update[$n]}
	${linux_install[$n]} unzip
fi
if [ -z $(type -P curl) ]; then
	${linux_update[$n]}
	${linux_install[$n]} curl
fi
if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') != "Alpine" ]; then
	if [ -z $(type -P systemctl) ]; then
		${linux_update[$n]}
		${linux_install[$n]} systemctl
	fi
fi

function quicktunnel() {

	read -p "是否升级cloudflared(默认0.跳过,1.升级): " isupdate
	if [ -z "$isupdate" ]; then
		if [ -f "cloudflared-linux" ]; then
			green "当前版本$(/opt/tunnel/cloudflared-linux -v | sed -n '1p')"
			isupdate=0
			echo 跳过升级cloudflared
		else
			isupdate=1
			echo 当前目录下缺少cloudflared-linux必要文件必须下载cloudflared-linux
		fi

	fi
	if [ $isupdate -eq 1 ]; then
		read -p "是否使用默认代理，是就直接回车: " proxyurl
		if [ -z "$proxyurl" ]; then
			$proxyurl="https://gh.api.99988866.xyz/"
		fi
		#创建主目录
		rm -rf cloudflared-linux
		case "$(uname -m)" in
		x86_64 | x64 | amd64)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o cloudflared-linux
			;;
		i386 | i686)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-386 -o cloudflared-linux
			;;
		armv8 | arm64 | aarch64)
			echo arm64
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm64 -o cloudflared-linux
			;;
		armv71)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm -o cloudflared-linux
			;;
		*)
			echo 当前架构$(uname -m)没有适配
			exit
			;;
		esac
		chmod +x cloudflared-linux
	fi
	read -p "输入需要代理的本地端口号: " port
	./cloudflared-linux tunnel --url http://localhost:$port --no-autoupdate --edge-ip-version $ips --protocol http2 > ${tunnel_home}/argo.log 2>&1 &
	sleep 1
	n=0
	while true; do
		n=$(($n + 1))
		clear
		echo 等待cloudflare argo生成地址 已等待 $n 秒
		argo=$(cat ${tunnel_home}/argo.log | grep trycloudflare.com | awk 'NR==2{print}' | awk -F// '{print $2}' | awk '{print $1}')
		if [ $n == 15 ]; then
			n=0
			if [ $addservice -eq 0 ]; then
				if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
					kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
				else
					kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
				fi
			fi
			rm -rf ${tunnel_home}/argo.log
			clear
			echo argo获取超时,重试中
			./cloudflared-linux tunnel --url http://localhost:$port --no-autoupdate --edge-ip-version $ips --protocol http2 > ${tunnel_home}/argo.log 2>&1 &
			sleep 1
		elif [ -z "$argo" ]; then
			sleep 1
		else
			rm -rf ${tunnel_home}/argo.log
			break
		fi
	done
	rm -rf ${tunnel_home}/argo.log
	echo 配置成功请访问 $argo
}

# 截取顶级域名并返回
get_tld() {
    local subdomain="$1"
    local tld=$(echo "$subdomain" | rev | cut -d. -f1-2 | rev)
	echo "$tld" | sed 's/\./_/g'
}

# 截取tunnel并返回
get_tunnel() {
    # 原始字符串
	original_string= /opt/tunnel/cloudflared-linux tunnel info $1

	# 使用grep命令和正则表达式提取tunnel部分
	tunnel=$(echo "$original_string" | grep -o -E 'tunnel [a-zA-Z0-9-]+')

	# 打印提取的tunnel部分
	echo "$tunnel"
}

function installtunnel() {
	read -p "是否升级cloudflared(默认0.跳过,1.升级): " isupdate
	if [ -z "$isupdate" ]; then
		if [ -f "/opt/tunnel/cloudflared-linux" ]; then
			isupdate=0
			echo 跳过升级cloudflared
		else
			isupdate=1
			echo 当前目录下缺少cloudflared-linux必要文件必须下载cloudflared-linux
		fi
	fi
	echo $isupdate
	if [ $isupdate == 1 ]; then
		echo "开始安装"
		read -p "是否使用代理: " proxyurl
		if [ -z "$proxyurl" ]; then
			proxyurl=""
		fi
		#创建主目录
		mkdir -p /opt/tunnel/ >/dev/null 2>&1
		rm -rf cloudflared-linux
		case "$(uname -m)" in
		x86_64 | x64 | amd64)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o cloudflared-linux
			;;
		i386 | i686)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-386 -o cloudflared-linux
			;;
		armv8 | arm64 | aarch64)
			echo arm64
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm64 -o cloudflared-linux
			;;
		armv71)
			curl -L ${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm -o cloudflared-linux
			;;
		*)
			echo 当前架构$(uname -m)没有适配
			exit
			;;
		esac
		chmod +x cloudflared-linux
		mv cloudflared-linux /opt/tunnel/
		clear
	fi

	echo 复制下面的链接,用浏览器打开并授权需要绑定的域名
	echo 在网页中授权完毕后会继续进行下一步设置
	/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel login
	clear
	/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel list > ${tunnel_home}/argo.log 2>&1
	echo -e ARGO TUNNEL当前已经绑定的服务如下'\n'
	sed 1,2d ${tunnel_home}/argo.log | awk '{print $2}'
	echo -e '\n'自定义一个完整二级域名,例如 xxx.example.com
	echo 必须是网页里面绑定授权的域名才生效,不能乱输入
	read -p "输入绑定域名的完整二级域名: " domain
	if [ -z "$domain" ]; then
		echo 没有设置域名
		exit
	elif [ $(echo $domain | grep "\." | wc -l) == 0 ]; then
		echo 域名格式不正确
		exit
	fi
	name=$(echo $domain | awk -F\. '{print $1}')
	
	if [ $addservice -eq 0 ]; then
		read -p "输入tunnel的路由名称: " route
		if [ -z $route ]; then
			route=$(date +%s%N | md5sum | cut -c 1-6)
		fi
		echo $route > ${tunnel_home}/route.log
	else
		route=$(cat ${tunnel_home}/route.log)	
	fi
	
	
	# 调用函数获取顶级域名并传入参数
	top_level_domain=$(get_tld $domain)
	tunnelid=$(get_tunnel $route)
	if [ $(sed 1,2d ${tunnel_home}/argo.log | awk '{print $2}' | grep -w $name | wc -l) == 0 ]; then
		if [ $addservice -eq 0 ]; then
			echo 创建TUNNEL $name
			/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel create $route > ${tunnel_home}/argo.log 2>&1
			echo TUNNEL $domain 创建成功
		fi
	else
		echo TUNNEL $name 已经存在
		if [ ! -f "/root/.cloudflared/$(sed 1,2d ${tunnel_home}/argo.log | awk '{print $1" "$2}' | grep -w $name | awk '{print $1}').json" ]; then
			echo /root/.cloudflared/$(sed 1,2d ${tunnel_home}/argo.log | awk '{print $1" "$2}' | grep -w $name | awk '{print $1}').json 文件不存在
			echo 清理TUNNEL $name
			/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel cleanup $name > ${tunnel_home}/argo.log 2>&1
			echo 删除TUNNEL $name
			/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel delete $name > ${tunnel_home}/argo.log 2>&1
			if [ $addservice -eq 0 ]; then
				echo 重建TUNNEL $name
				/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel create $route > ${tunnel_home}/argo.log 2>&1
			fi
			cat ${tunnel_home}/argo.log
		else
			echo 清理TUNNEL $name
			/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel cleanup $name > ${tunnel_home}/argo.log 2>&1
		fi
	fi
	echo 绑定 TUNNEL $name 到域名 $domain
	/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel route dns --overwrite-dns $route $domain > ${tunnel_home}/argo.log 2>&1
	echo $domain 绑定成功
	tunneluuid=$(cut -d= -f2 ${tunnel_home}/argo.log)
	echo $(cat ${tunnel_home}/argo.log)
	if [ -z "$tunneluuid" ]; then
		tunneluuid=$tunnelid
	fi
	#rm -rf argo.log
	read -p "输入需要代理的本地端口号: " port
	read -p "输入需要代理的本地端口号对应的协议，如http|ws|ssh|...: " proxyprotocol
	if [ -z "$proxyprotocol" ];then
		proxyprotocol=http
	fi
	
	if [ $addservice -eq 0 ]; then
		cat >/opt/tunnel/config.yaml <<EOF
tunnel: $tunneluuid
credentials-file: /root/.cloudflared/$tunneluuid.json

ingress:
  - hostname: $domain
    service: $proxyprotocol://localhost:$port
  - service: http_status:404
EOF
		if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
			cat >/etc/local.d/cloudflared.start <<EOF
/opt/tunnel/cloudflared-linux --edge-ip-version $ips tunnel --config /opt/tunnel/config.yaml run &
EOF
			chmod +x /etc/local.d/cloudflared.start
			rc-update add local
			/etc/local.d/cloudflared.start >/dev/null 2>&1
		else
			#创建服务
			cat >/lib/systemd/system/cloudflared.service <<EOF
[Unit]
Description=Cloudflare Tunnel
After=network.target

[Service]
TimeoutStartSec=0
Type=simple
ExecStart=/opt/tunnel/cloudflared-linux --edge-ip-version $ips tunnel  --config /opt/tunnel/config.yaml run
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
EOF
			systemctl enable cloudflared.service >/dev/null 2>&1
			systemctl --system daemon-reload
			systemctl start cloudflared.service
		fi
		if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
			#创建命令链接
			cat >/opt/tunnel/zdtunnel.sh <<EOF
#!/bin/bash
green() { echo -e "\033[32m\033[01m$1\033[0m"; }
tunnel_home="/opt/tunnel"

function quicktunnel() {

	read -p "是否升级cloudflared(默认0.跳过,1.升级): " isupdate
	if [ -z "\$isupdate" ]; then
		if [ -f "/opt/tunnel/cloudflared-linux" ]; then
			green "当前版本\$(/opt/tunnel/cloudflared-linux -v | sed -n '1p')"
			isupdate=0
			echo 跳过升级cloudflared
		else
			isupdate=1
			echo 当前目录下缺少cloudflared-linux必要文件必须下载cloudflared-linux
		fi

	fi
	if [ \$isupdate -eq 1 ]; then
	
		read -p "是否使用默认代理，是就直接回车: " proxyurl
		if [ -z "\$proxyurl" ]; then
			proxyurl="https://gh.api.99988866.xyz/"
		fi
		#创建主目录
		rm -rf cloudflared-linux
		case "\$(uname -m)" in
		x86_64 | x64 | amd64)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o cloudflared-linux
			;;
		i386 | i686)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-386 -o cloudflared-linux
			;;
		armv8 | arm64 | aarch64)
			echo arm64
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm64 -o cloudflared-linux
			;;
		armv71)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm -o cloudflared-linux
			;;
		*)
			echo 当前架构\$(uname -m)没有适配
			exit
			;;
		esac
		chmod +x cloudflared-linux
	fi
}

function addService() {

	read -p "请选择argo连接模式IPV4或者IPV6(输入4或6,默认4):" ips
	if [ -z "\$ips" ]; then
		ips=4
	fi

	/opt/tunnel/cloudflared-linux --edge-ip-version \$ips --protocol http2 tunnel list > \${tunnel_home}/argo.log 2>&1
	echo -e ARGO TUNNEL当前已经绑定的服务如下'\n'
	sed 1,2d \${tunnel_home}/argo.log | awk '{print \$2}'
	echo -e '\n'自定义一个完整二级域名,例如 xxx.example.com
	echo 必须是网页里面绑定授权的域名才生效,不能乱输入
	read -p "输入绑定域名的完整二级域名: " domain
	if [ -z "\$domain" ]; then
		echo 没有设置域名
		exit
	elif [ \$(echo \$domain | grep "\." | wc -l) == 0 ]; then
		echo 域名格式不正确
		exit
	fi
	name=\$(echo \$domain | awk -F\. '{print \$1}')
	
	route=\$(cat \${tunnel_home}/route.log)
	
	
	# 调用函数获取顶级域名并传入参数
	tunnelid=\$(get_tunnel \$route)
	echo 绑定 TUNNEL \$name 到域名 \$domain
	/opt/tunnel/cloudflared-linux --edge-ip-version \$ips --protocol http2 tunnel route dns --overwrite-dns \$route \$domain > \${tunnel_home}/argo.log 2>&1
	echo \$domain 绑定成功
	tunneluuid=\$(cut -d= -f2 \${tunnel_home}/argo.log)
	echo \$(cat \${tunnel_home}/argo.log)
	if [ -z "\$tunneluuid" ]; then
		tunneluuid=\$tunnelid
	fi
	#rm -rf argo.log
	read -p "输入需要代理的本地端口号: " port
	read -p "输入需要代理的本地端口号对应的协议，如http|ws|ssh|...: " proxyprotocol
	if [ -z "\$proxyprotocol" ];then
		proxyprotocol=http
	fi
	if ! grep -q "\$domain" /opt/tunnel/config.yaml;then	
		# 要匹配的字符串
		search_string="hostname:"

		# 要插入的新内容
		new_content="  - hostname: "\$domain"\n    service: \${proxyprotocol}://localhost:"\$port

		# 使用 sed 命令在 config.yaml 文件中进行匹配和插入
		# 使用 awk 命令在匹配的行的上一行插入新内容，并将结果保存到新文件
		awk -v ss="\$search_string" -v nc="\$new_content" '/'"\$search_string"'/{if(!f){print nc;f=1}print;next}1' /opt/tunnel/config.yaml >/opt/tunnel/config.tmp && mv /opt/tunnel/config.tmp /opt/tunnel/config.yaml
		systemctl restart cloudflared.service
	fi
	green 此次新增：\$domain端口号为：\$port
	cat /opt/tunnel/config.yaml 
}

while true
do
if [ \$(ps -ef | grep cloudflared-linux | grep -v grep | wc -l) == 0 ]
then
	argostatus=stop
else
	argostatus=running
fi
echo argo \$argostatus
echo 1.管理TUNNEL
echo 2.启动服务
echo 3.停止服务
echo 4.重启服务
echo 5.卸载服务
echo 6.查看当前v2ray链接
echo 7.更新cloudflare
echo 0.退出
read -p "请选择菜单(默认0): " menu
if [ -z "\$menu" ]
then
	menu=0
fi
if [ \$menu == 1 ]
then
	clear
	while true
	do
		echo ARGO TUNNEL当前已经绑定的服务如下
		/opt/tunnel/cloudflared-linux tunnel list
		echo 2.增加TUNNEL
		echo 1.删除TUNNEL
		echo 0.退出
		read -p "请选择菜单(默认0): " tunneladmin
		if [ -z "\$tunneladmin" ]
		then
			tunneladmin=0
		fi
		if [ \$tunneladmin == 1 ]
		then
			read -p "请输入要删除的TUNNEL NAME: " tunnelname
			echo 断开TUNNEL \$tunnelname
			/opt/tunnel/cloudflared-linux tunnel cleanup \$tunnelname
			echo 删除TUNNEL \$tunnelname
			/opt/tunnel/cloudflared-linux tunnel delete \$tunnelname
		elif [ \$tunneladmin == 2 ]
		then
			addService
		else
			break
		fi
	done
elif [ \$menu == 2 ]
then
	kill -9 \$(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print \$1}') >/dev/null 2>&1
	/etc/local.d/cloudflared.start >/dev/null 2>&1
	clear
	sleep 1
elif [ \$menu == 3 ]
then
	kill -9 \$(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print \$1}') >/dev/null 2>&1
	clear
	sleep 2
elif [ \$menu == 4 ]
then
	kill -9 \$(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print \$1}') >/dev/null 2>&1
	/etc/local.d/cloudflared.start >/dev/null 2>&1
	clear
	sleep 1
elif [ \$menu == 5 ]
then
	kill -9 \$(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print \$1}') >/dev/null 2>&1
	rm -rf /opt/tunnel /etc/local.d/cloudflared.start /usr/bin/zdtunnel ~/.cloudflared
	echo 所有服务都卸载完成
	echo 彻底删除授权记录
	echo 请访问 https://dash.cloudflare.com/profile/api-tokens
	echo 删除授权的 Argo Tunnel API Token 即可
	exit
elif [ \$menu == 6 ]
then
	clear
	cat /opt/tunnel/v2ray.txt
elif [ \$menu == 7 ]
then
	systemctl stop cloudflared.service
	quicktunnel
	clear
	sleep 2
elif [ \$menu == 0 ]
then
	echo 退出成功
	exit
fi
done
EOF
		#上面是aplin系统的
		else
			#创建命令链接
			cat >/opt/tunnel/zdtunnel.sh <<EOF
#!/bin/bash
clear
green() { echo -e "\033[32m\033[01m$1\033[0m"; }
tunnel_home="/opt/tunnel"

function quicktunnel() {

	read -p "是否升级cloudflared(默认0.跳过,1.升级): " isupdate
	if [ -z "\$isupdate" ]; then
		if [ -f "/opt/tunnel/cloudflared-linux" ]; then
			green "当前版本$(/opt/tunnel/cloudflared-linux -v | sed -n '1p')"
			isupdate=0
			echo 跳过升级cloudflared
		else
			isupdate=1
			echo 当前目录下缺少cloudflared-linux必要文件必须下载cloudflared-linux
		fi

	fi
	if [ \$isupdate -eq 1 ]; then
	
		read -p "是否使用默认代理，是就直接回车: " proxyurl
		if [ -z "\$proxyurl" ]; then
			proxyurl="https://gh.api.99988866.xyz/"
		fi
		#创建主目录
		rm -rf cloudflared-linux
		case "\$(uname -m)" in
		x86_64 | x64 | amd64)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 -o cloudflared-linux
			;;
		i386 | i686)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-386 -o cloudflared-linux
			;;
		armv8 | arm64 | aarch64)
			echo arm64
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm64 -o cloudflared-linux
			;;
		armv71)
			curl -L \${proxyurl}https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm -o cloudflared-linux
			;;
		*)
			echo 当前架构\$(uname -m)没有适配
			exit
			;;
		esac
		chmod +x cloudflared-linux
	fi
}

# 截取tunnel并返回
get_tunnel() {
    # 原始字符串
	original_string= /opt/tunnel/cloudflared-linux tunnel info \$1

	# 使用grep命令和正则表达式提取tunnel部分
	echo $(echo "\$original_string" | grep -o -E 'tunnel [a-zA-Z0-9-]+')
}

function addService() {

	read -p "请选择argo连接模式IPV4或者IPV6(输入4或6,默认4):" ips
	if [ -z "\$ips" ]; then
		ips=4
	fi

	/opt/tunnel/cloudflared-linux --edge-ip-version $ips --protocol http2 tunnel list > \${tunnel_home}/argo.log 2>&1
	echo -e ARGO TUNNEL当前已经绑定的服务如下'\n'
	sed 1,2d \${tunnel_home}/argo.log | awk '{print \$2}'
	echo -e '\n'自定义一个完整二级域名,例如 xxx.example.com
	echo 必须是网页里面绑定授权的域名才生效,不能乱输入
	read -p "输入绑定域名的完整二级域名: " domain
	if [ -z "\$domain" ]; then
		echo 没有设置域名
		exit
	elif [ \$(echo \$domain | grep "\." | wc -l) == 0 ]; then
		echo 域名格式不正确
		exit
	fi
	name=\$(echo \$domain | awk -F\. '{print \$1}')
	
	route=\$(cat \${tunnel_home}/route.log)
	
	
	# 调用函数获取顶级域名并传入参数
	tunnelid=\$(get_tunnel \$route)
	echo 绑定 TUNNEL \$name 到域名 \$domain
	/opt/tunnel/cloudflared-linux --edge-ip-version \$ips --protocol http2 tunnel route dns --overwrite-dns \$route \$domain > \${tunnel_home}/argo.log 2>&1
	echo $domain 绑定成功
	tunneluuid=\$(cut -d= -f2 \${tunnel_home}/argo.log)
	echo $(cat \${tunnel_home}/argo.log)
	if [ -z "\$tunneluuid" ]; then
		tunneluuid=\$tunnelid
	fi
	#rm -rf argo.log
	read -p "输入需要代理的本地端口号: " port
	read -p "输入需要代理的本地端口号对应的协议，如http|ws|ssh|...: " proxyprotocol
	if [ -z "\$proxyprotocol" ];then
		proxyprotocol=http
	fi
	if ! grep -q "\$domain" /opt/tunnel/config.yaml;then	
		# 要匹配的字符串
		search_string="hostname:"

		# 要插入的新内容
		new_content="  - hostname: "\$domain"\n    service: \${proxyprotocol}://localhost:"\$port

		# 使用 sed 命令在 config.yaml 文件中进行匹配和插入
		# 使用 awk 命令在匹配的行的上一行插入新内容，并将结果保存到新文件
		awk -v ss="\$search_string" -v nc="\$new_content" '/'"\$search_string"'/{if(!f){print nc;f=1}print;next}1' /opt/tunnel/config.yaml >/opt/tunnel/config.tmp && mv /opt/tunnel/config.tmp /opt/tunnel/config.yaml
		systemctl restart cloudflared.service
	fi
	green 此次新增：\$domain端口号为：\$port
	cat /opt/tunnel/config.yaml 
}

while true
do
echo argo \$(systemctl status cloudflared.service | sed -n '3p')
echo "当前版本\$(/opt/tunnel/cloudflared-linux -v | sed -n '1p')"
echo 1.管理TUNNEL
echo 2.启动服务
echo 3.停止服务
echo 4.重启服务
echo 5.卸载服务
echo 6.查看当前v2ray链接
echo 7.更新cloudflare
echo 0.退出
read -p "请选择菜单(默认0): " menu
if [ -z "\$menu" ]
then
	menu=0
fi
if [ \$menu == 1 ]
then
	clear
	while true
	do
		echo ARGO TUNNEL当前已经绑定的服务如下
		/opt/tunnel/cloudflared-linux tunnel list
		echo 2.增加TUNNEL
		echo 1.删除TUNNEL
		echo 0.退出
		read -p "请选择菜单(默认0): " tunneladmin
		if [ -z "\$tunneladmin" ]
		then
			tunneladmin=0
		fi
		if [ \$tunneladmin == 1 ]
		then
			read -p "请输入要删除的TUNNEL NAME: " tunnelname
			echo 断开TUNNEL \$tunnelname
			/opt/tunnel/cloudflared-linux tunnel cleanup \$tunnelname
			echo 删除TUNNEL \$tunnelname
			/opt/tunnel/cloudflared-linux tunnel delete \$tunnelname
		elif [ \$tunneladmin == 2 ]
		then
			addService
		else
			break
		fi
	done
elif [ \$menu == 2 ]
then
	systemctl start cloudflared.service
	clear
elif [ \$menu == 3 ]
then
	systemctl stop cloudflared.service
	clear
elif [ \$menu == 4 ]
then
	systemctl restart cloudflared.service
	clear
elif [ \$menu == 5 ]
then
	systemctl stop cloudflared.service
	systemctl disable cloudflared.service
	kill -9 \$(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print \$2}') >/dev/null 2>&1
	rm -rf /opt/tunnel /lib/systemd/system/cloudflared.service /usr/bin/zdtunnel ~/.cloudflared
	systemctl --system daemon-reload
	echo 所有服务都卸载完成
	echo 彻底删除授权记录
	echo 请访问 https://dash.cloudflare.com/profile/api-tokens
	echo 删除授权的 Argo Tunnel API Token 即可
	exit
elif [ \$menu == 6 ]
then
	clear
	cat /opt/tunnel/v2ray.txt
elif [ \$menu == 7 ]
then
	systemctl stop cloudflared.service
	quicktunnel
	clear
	sleep 2
elif [ \$menu == 0 ]
then
	echo 退出成功
	exit
fi
done
EOF
		fi
		chmod +x /opt/tunnel/zdtunnel.sh
		ln -sf /opt/tunnel/zdtunnel.sh /usr/bin/zdtunnel
	else
		if ! grep -q "$domain" /opt/tunnel/config.yaml;then	
			# 要匹配的字符串
			search_string="hostname:"

			# 要插入的新内容
			new_content="  - hostname: "$domain"\n    service: ${proxyprotocol}://localhost:"$port

			# 使用 sed 命令在 config.yaml 文件中进行匹配和插入
			# 使用 awk 命令在匹配的行的上一行插入新内容，并将结果保存到新文件
			awk -v ss="$search_string" -v nc="$new_content" '/'"$search_string"'/{if(!f){print nc;f=1}print;next}1' /opt/tunnel/config.yaml >/opt/tunnel/config.tmp && mv /opt/tunnel/config.tmp /opt/tunnel/config.yaml
			systemctl restart cloudflared.service
		fi
	fi
	green 此次新增：$domain端口号为：$port
	cat /opt/tunnel/config.yaml 
}

clear
echo 梭哈模式不需要自己提供域名,使用CF ARGO QUICK TUNNEL创建快速链接
echo 梭哈模式在重启或者脚本再次运行后失效,如果需要使用需要再次运行创建
echo 安装服务模式,需要有CF托管域名,并且需要按照提示手动绑定ARGO服务
echo 首次绑定ARGO服务后如果不想再次跳转网页绑定
echo 将已经绑定的系统目录下的 /root/.cloudflared 文件夹以及内容
echo 拷贝至新系统下同样的目录,会自动跳过登录验证

echo -e '\n'梭哈是一种智慧!!!梭哈!梭哈!梭哈!梭哈!梭哈!梭哈!梭哈...'\n'
echo 1.梭哈模式
echo 2.安装服务
echo 3.卸载服务
echo 4.清空缓存
echo -e 0.退出脚本'\n'
read -p "请选择模式(默认1):" mode
if [ -z "$mode" ]; then
	mode=1
fi
if [ $mode == 1 ]; then
	read -p "请选择argo连接模式IPV4或者IPV6(输入4或6,默认4):" ips
	if [ -z "$ips" ]; then
		ips=4
	fi
	if [ $ips != 4 ] && [ $ips != 6 ]; then
		echo 请输入正确的argo连接模式
		exit
	fi
	read -p "是新建还是增加服务(输入0.新增或1.增加,默认0):" addservice
	if [ -z "$addservice" ]; then
		addservice=0
	fi
	if [ $addservice -eq 0 ]; then
		if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
		else
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
		fi
	fi

	quicktunnel
elif [ $mode == 2 ]; then
	read -p "请选择argo连接模式IPV4或者IPV6(输入4或6,默认4):" ips
	if [ -z "$ips" ]; then
		ips=4
	fi
	if [ $ips != 4 ] && [ $ips != 6 ]; then
		echo 请输入正确的argo连接模式
		exit
	fi
	
	read -p "是新建还是增加服务(输入0.新增或1.增加,默认0):" addservice
	if [ -z "$addservice" ]; then
		addservice=0
	fi
	#isp=$(curl -$ips -s https://speed.cloudflare.com/meta | awk -F\" '{print $26"-"$18"-"$30}' | sed -e 's/ /_/g')
	if [ $addservice -eq 0 ]; then
		if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
			rm -rf /opt/tunnel /lib/systemd/system/cloudflared.service /usr/bin/zdtunnel
		else
			systemctl stop cloudflared.service
			systemctl disable cloudflared.service
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
			rm -rf /opt/tunnel /lib/systemd/system/cloudflared.service /usr/bin/zdtunnel
			systemctl --system daemon-reload
		fi
	else
		if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
		else
			kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
		fi
	fi
	installtunnel
elif [ $mode == 3 ]; then
	if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
		kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
		rm -rf /opt/tunnel /lib/systemd/system/cloudflared.service /lib/systemd/system/xray.service /usr/bin/zdtunnel
	else
		systemctl stop cloudflared.service
		systemctl disable cloudflared.service
		kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
		rm -rf /opt/tunnel /lib/systemd/system/cloudflared.service /usr/bin/zdtunnel ~/.cloudflared
		systemctl --system daemon-reload
	fi
	clear
	echo 所有服务都卸载完成
	echo 彻底删除授权记录
	echo 请访问 https://dash.cloudflare.com/profile/api-tokens
	echo 删除授权的 Argo Tunnel API Token 即可
elif [ $mode == 4 ]; then
	if [ $(grep -i PRETTY_NAME /etc/os-release | cut -d \" -f2 | awk '{print $1}') == "Alpine" ]; then
		kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $1}') >/dev/null 2>&1
	else
		kill -9 $(ps -ef | grep cloudflared-linux | grep -v grep | awk '{print $2}') >/dev/null 2>&1
	fi
	rm -rf xray cloudflared-linux v2ray.txt
else
	echo 退出成功
	exit
fi
